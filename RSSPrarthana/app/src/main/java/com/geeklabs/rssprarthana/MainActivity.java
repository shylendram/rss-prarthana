package com.geeklabs.rssprarthana;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

/**
 * Created by Shailendra Madda on 28-July-15.
 */

public class MainActivity extends AppCompatActivity {

    private TextView prayerText, songCurrentDurationLabel, songTotalDurationLabel;
    private boolean doubleBackToExitPressedOnce;
    private FloatingActionButton play_pause_button;
    private SeekBar seekBar;
    private MediaPlayer mp;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Keep screen active
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        play_pause_button = findViewById(R.id.play_button);
        songCurrentDurationLabel = findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = findViewById(R.id.songTotalDurationLabel);

        showToast("Pinch with two fingers to zoom the text");
        initAdds();
        mHandler = new Handler();
        initMediaPlayer();

        final Handler mHandler = new Handler();
        // Make sure you update Seek bar on UI thread
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (mp != null) {
                    int mCurrentPosition = mp.getCurrentPosition();
                    seekBar.setProgress(mCurrentPosition);
                }
                mHandler.postDelayed(this, 1000);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mp != null && fromUser) {
                    mp.seekTo(progress);
                    mp.start();
                    play_pause_button.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_pause));
                }

            }
        });

        play_pause_button.setOnClickListener(v -> {
            if (mp != null) {
                if (mp.isPlaying()) {
                    mp.pause();
                    play_pause_button.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_play));
                } else {
                    play_pause_button.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_pause));
                    mp.start();
                }
            }
        });

        // Listen call states
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        PhoneStateListener callStateListener = new PhoneStateListener() {
            boolean toTrack = false; // to prevent triggering in onCreate

            public void onCallStateChanged(int state, String incomingNumber) {
                // If phone is Ringing
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    Log.d("MainActivity", "CALL_STATE_RINGING");
                    if (mp != null && mp.isPlaying()) {
                        mp.pause();
                        play_pause_button.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_play));
                    }
                }
                // If Picked and is Currently in a call
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    Log.d("MainActivity", "CALL_STATE_OFFHOOK means in a call now");
                    if (toTrack && mp != null && mp.isPlaying()) {
                        mp.pause();
                        play_pause_button.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_play));
                    }
                    toTrack = true;
                }
                // If not picked or picked
                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    Log.d("MainActivity", "CALL_STATE_IDLE means in idle state");
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };

        if (telephonyManager != null) {
            telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }

    private void initAdds() {
        MobileAds.initialize(this, initializationStatus -> {
            AdView adView = findViewById(R.id.adView);
        });
    }

    private void initMediaPlayer() {
        mp = new MediaPlayer();
        mp.reset();
        prayerText = findViewById(R.id.prayerText);
        seekBar = findViewById(R.id.seekBar);
        try {
            AssetFileDescriptor descriptor = getAssets().openFd("Prarthana.mp3");
            mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp.prepare();
            mp.setLooping(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int duration = mp.getDuration();
        seekBar.setMax(duration);
        // Play prayer
        if (mp != null) {
            mp.start();
            mUpdateTimeTask.run();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.mainActivity);
        switch (item.getItemId()) {
            case R.id.shareApp:
                String shareTitle = "RSS Prarthana in different languages with meaning.";
                String shareBody = shareTitle + " Click the below link to install it from Google play store.\n\nhttps://play.google.com/store/apps/details?id=com.geeklabs.rssprarthana&hl=en";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareTitle);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case R.id.telugu:
                prayerText.setText(getText(R.string.teluguPrayer));
                setTitle("RSS Prarthana (Telugu)");
                break;
            case R.id.hindi:
                prayerText.setText(getText(R.string.hindiPrayer));
                setTitle("RSS Prarthana (Hindi)");
                break;
            case R.id.english:
                prayerText.setText(getText(R.string.englishPrayer));
                setTitle("RSS Prarthana (English)");
                break;
            case R.id.malayalam:
                prayerText.setText(getText(R.string.malayalamPrayer));
                setTitle("RSS Prarthana (Malayalam)");
                break;
            case R.id.kannada:
                prayerText.setText(getText(R.string.kannadaPrayer));
                setTitle("RSS Prarthana (Kannada)");
                break;
            case R.id.bangla:
                prayerText.setText(getText(R.string.banglaPrayer));
                setTitle("RSS Prarthana (Bangla)");
                break;
            case R.id.tamil:
                prayerText.setText(getText(R.string.tamilPrayer));
                setTitle("RSS Prarthana (Tamil)");
                break;
            case R.id.gujarati:
                prayerText.setText(getText(R.string.gujaratiPrayer));
                setTitle("RSS Prarthana (Gujarati)");
                break;
            case R.id.gurumukhi:
                prayerText.setText(getText(R.string.gurumukhiPrayer));
                setTitle("RSS Prarthana (Gurumuki)");
                break;
            case R.id.bengali:
                prayerText.setText(getText(R.string.bengaliPrayer));
                setTitle("RSS Prarthana (Bengali)");
                break;
            case R.id.odia:
                prayerText.setText(getText(R.string.odiaPrayer));
                setTitle("RSS Prarthana (Odia)");
                break;
            case R.id.meaning:
                prayerText.setText(getText(R.string.meaningText));
                setTitle("RSS Prarthana (Meaning)");
                // change back ground image
                /*Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.flag);
                linearLayout.setBackground(drawable);*/
                break;
            case R.id.joinRss:
                startActivity(new Intent(this, WebViewActivity.class)
                        .putExtra("webUrl", "http://rss.org/pages/joinrss.aspx"));
                break;
            case R.id.rateApp:
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException e) {
                    e.printStackTrace();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast("Unable to open app in play store");
                }
                break;
            case R.id.vijayaVipanchi:
                startActivity(new Intent(this, WebViewActivity.class)
                        .putExtra("webUrl", "http://vijayavipanchi.org/SongsByGenre.aspx?GenreID=1"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        @Override
        public void run() {
            long totalDuration = mp.getDuration();
            long currentDuration = mp.getCurrentPosition();
            // Displaying Total Duration time
            songTotalDurationLabel.setText(String.format("%s", milliSecondsToTimer(totalDuration)));
            // Displaying time completed playing
            songCurrentDurationLabel.setText(String.format("%s", milliSecondsToTimer(currentDuration)));
            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString;
        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }
        finalTimerString = finalTimerString + minutes + ":" + secondsString;
        // return timer string
        return finalTimerString;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            finishAffinity();
            return;
        }
        doubleBackToExitPressedOnce = true;
        showToast("Tap BACK again to exit");
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}

