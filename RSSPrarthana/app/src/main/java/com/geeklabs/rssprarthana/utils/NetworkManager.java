package com.geeklabs.rssprarthana.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;

import java.util.Objects;

/**
 * Created by Shailendra Madda on 04/5/2017.
 */

public class NetworkManager {

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        assert connectivityManager != null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network activeNetwork = connectivityManager.getActiveNetwork();
            if (activeNetwork != null) {
                NetworkCapabilities nc = connectivityManager.getNetworkCapabilities(activeNetwork);
                assert nc != null;
                return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                        NetworkCapabilities.TRANSPORT_WIFI
                );
            }
            return false;
        } else {
            return Objects.requireNonNull(connectivityManager.getActiveNetworkInfo()).isConnected();
        }
    }

}
